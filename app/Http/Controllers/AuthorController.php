<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Setting;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Intervention\Image\Drivers\Gd\Driver;
use Intervention\Image\Facades\Image;


class AuthorController extends Controller
{

    public function index(){
        return view('back.pages.home');
    }

    public function logout(){
        Auth::guard('web')->logout();
        return redirect()->route('author.login');
    }

    public function ResetForm(Request $request ,$token = null){
        $data = [
            'pageTitle'=>'Reset Password'
        ];
        return redirect()->route('back.pages.auth.reset'.$data)->with(['token'=>$token, 'email'=>$request->email]);
    }

    public function changeProfilePicture(Request $request){
        $user = User::find(auth('web')->id());
        $path = 'back/dist/img/authors';
        $file = $request->file('file');
        $old_picture = $user->getAttributes()['picture'];
        $file_path = $path.$old_picture;
        $new_picture_name = 'AIMG'.$user->id.time().rand(1,100000).'.jpg';

        if ( $new_picture_name != null && File::exists(public_path($file_path))) {
            File::delete(public_path($file_path));
        }
        $upload = $file->move(public_path($path), $new_picture_name);
        if ($upload) {
            $user->update([
                'picture'=>$new_picture_name
            ]);
            return session()->flash('success', 'your profile picture has been successfullt updated.');
        }else {
            return session()->flash('fail', 'something went wrong.');

        }
    }


    public function changeBlogLogo(Request $request){
        $settings = Setting::find(1);
        $logo_path = 'back/dist/img/logo-favicon';
        $old_logo = $settings->getAttributes()['blog_logo'];
        $file = $request->file('file');
        $filename = time().'_'.rand(1,100000).'_larablog.png';
        if ($request->hasFile('blog_logo')){
            if ($old_logo != null && File::exists(public_path($logo_path.$old_logo))) {
                File::delete(public_path($logo_path.$old_logo));
            }
            $upload = $file->move(public_path($logo_path), $filename);
            if ($upload) {
                $settings->update([
                    'blog_logo'=>$filename
                ]);
                return session()->flash('success', 'your profile picture has been successfullt updated.');
            }else {
                return session()->flash('fail', 'something went wrong.');

            }
        }
    }

    public function createPost(Request $request)
    {
        $post_category_rule = 'required|exists:sub_categories,id'; // Assuming the table name is 'sub_categories'

        $request->validate([
            'post_title' => 'required|unique:posts,post_title',
            'post_content' => 'required',
            'post_category' => $post_category_rule,
            'featured_image' => 'required|mimes:jpg,jpeg,png|max:1024',
        ]);

        if ($request->hasFile('featured_image')){
            $file = $request->file('featured_image');
            $filename = time() . '_' . $file->getClientOriginalName();

            // Move the uploaded file to the public directory
            $file->move(public_path('images/post_images'), $filename);

            $path = "images/post_images/";
            $new_filename = $filename;

            // $upload = Storage::disk('public')->put($path.$new_filename, (string) file_get_contents($file));

            // $post_thumbnails_path = $path.'thumbnails';
            // if ( !Storage::disk('public')->exists($post_thumbnails_path) ) {
            //     Storage::disk('public')->makeDirectory($post_thumbnails_path, 0755, true, true);
            // }

            // $imageManager = new ImageManager(new Driver());
            // // Create square thumbnail
            // $imageManager->make(storage_path('app/public/'.$path.$new_filename))
            //     ->fit(200, 200)
            //     ->save(storage_path('app/public/'.$path.'thumbnails/'.'thumb_'.$new_filename));

            // // Create resized image
            // $imageManager->make(storage_path('app/public/'.$path.$new_filename))
            //     ->fit(500, 350)
            //     ->save(storage_path('app/public/'.$path.'thumbnails/'.'resized_'.$new_filename));


                $post = new Post();
                $post->author_id = auth()->id();
                $post->category_id = $request->post_category;
                $post->post_title = $request->post_title;
                // $post->post_slug = Str::slug($request->post_title);
                $post->post_content = $request->post_content;
                $post->featured_image = $new_filename;
                $saved = $post->save();

                if ($saved) {
                    return redirect()->back()->with('success', 'New Post has been successfully created.');
                    // return redirect()->back()->with('success', 'New Post has been successfuly created.');
                    // return session()->flash('success', 'New Post has been successfuly created.');
                } else {
                    return redirect()->back()->with('error', 'Something went wrong.');
                    // return session()->flash('faile', 'something went wrong.');
                }


        }else {
            return redirect()->back()->with('error', 'Please select a featured image.');
        }

    }

    // public function createPost(Request $request)
    // {
    //     $request->validate([
    //         'post_title' => 'required|unique:posts,post_title',
    //         'post_content' => 'required',
    //         'post_category' => 'required|exists:sub_categories,id',
    //         'featured_image' => 'required|mimes:jpg,jpeg,png|max:1024',
    //     ]);

    //     if ($request->hasFile('featured_image')) {
    //         $file = $request->file('featured_image');
    //         $filename = time() . '_' . $file->getClientOriginalName();

    //         // Move the uploaded file to the public directory
    //         $file->move(public_path('images/post_images'), $filename);

    //         $post = new Post();
    //         $post->author_id = auth()->id();
    //         $post->category_id = $request->post_category;
    //         $post->post_title = $request->post_title;
    //         $post->post_content = $request->post_content;
    //         $post->featured_image = $filename; // Save just the filename
    //         $saved = $post->save();

    //         if ($saved) {
    //             return redirect()->back()->with('success', 'New Post has been successfully created.');
    //         } else {
    //             return redirect()->back()->with('error', 'Something went wrong.');
    //         }
    //     } else {
    //         return redirect()->back()->with('error', 'Please select a featured image.');
    //     }
    // }

    // public function updatePost(Request $request)
    // {
    //     $request->validate([
    //         'post_title' => 'required|unique:posts,post_title,' . $request->post_id,
    //         'post_content' => 'required',
    //         'post_category' => 'required|exists:sub_categories,id',
    //     ]);

    //     $post = Post::find($request->post_id);
    //     if (!$post) {
    //         return response()->json(['code' => 3, 'msg' => 'Post not found.']);
    //     }

    //     $post->category_id = $request->post_category;
    //     $post->post_title = $request->post_title;
    //     $post->post_content = $request->post_content;

    //     if ($request->hasFile('featured_image')) {
    //         $file = $request->file('featured_image');
    //         $filename = time() . '_' . $file->getClientOriginalName();

    //         // Move the uploaded file to the public directory
    //         $file->move(public_path('images/post_images'), $filename);

    //         $post->featured_image = $filename; // Update the featured image filename
    //     }

    //     $saved = $post->save();

    //     if ($saved) {
    //         return response()->json(['code' => 1, 'msg' => 'Post has been successfully updated.']);
    //     } else {
    //         return response()->json(['code' => 3, 'msg' => 'Something went wrong.']);
    //     }
    // }


    public function editPost(Request $request){
        if( !request()->post_id)  {
            return abort(404);
        } else {
            $post = Post::find(request()->post_id);
            $data = [
                'post'=>$post,
                'pageTitle'=>'Edit Page',
            ];
            return view('back.pages.edit_post',$data);
        }

    }
    public function updatePost(Request $request){
        if( $request->hasFile('featured_image'))  {

            $request->validate([
                'post_title' => 'required|unique:posts,post_title,'.$request->post_id,
                'post_content' => 'required',
                'post_category' => 'required|exists:categories,id',
                'featured_image' => 'required|mimes:jpg,jpeg,png|max:1024',
            ]);
            $file = $request->file('featured_image');
            $filename = time() . '_' . $file->getClientOriginalName();

            // Move the uploaded file to the public directory
            $file->move(public_path('images/post_images'), $filename);

            $path = "images/post_images/";
            $new_filename = $filename;

            $post_thumbnails_path = public_path($path . 'thumbnails');
            if (!File::exists($post_thumbnails_path)) {
                File::makeDirectory($post_thumbnails_path, 0755, true, true);
            }
            $post = Post::find($request->post_id);
            $post->author_id = auth()->id();
            $post->category_id = $request->post_category;
            $post->post_title = $request->post_title;
            $post->post_slug = null;
            $post->post_content = $request->post_content;
            $post->featured_image = $new_filename;
            $saved = $post->save();

            if ($saved) {
                return response()->json(['code' => 1, 'msg' => 'New Post has been successfully updated.']);
                // session()->flash('success', 'New SubCategory has been added successfuly.');

            } else {
                return response()->json(['code' => 3, 'msg' => 'Something went wrong.']);
            }

        }else{
            $request->validate([
                'post_title' => 'required|unique:posts,post_title,'.$request->post_id,
                'post_content' => 'required',
                'post_category' => 'required|exists:sub_categories,id',
            ]);

            $post = Post::find($request->post_id);
            $post->category_id = $request->post_category;
            $post->post_slug = null;
            $post->post_content = $request->post_content;
            $post->post_title = $request->post_title;
            $saved = $post->save();

            if ($saved) {
                return response()->json(['code' => 1, 'msg' => 'New Post has been successfully updated.']);
            } else {
                return response()->json(['code' => 3, 'msg' => 'Something went wrong.']);
            }
        }

    }



}
