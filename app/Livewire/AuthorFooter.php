<?php

namespace App\Livewire;

use App\Models\Setting;
use Livewire\Component;

class AuthorFooter extends Component
{
    protected $listeners = [
        'updateAuthorFooter'=>'$refresh'
    ];

    public $settings;

    public function mount(){
        $this->settings = Setting::find(1);
    }

    public function render()
    {
        return view('livewire.author-footer');
    }
}
