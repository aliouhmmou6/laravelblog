<?php

namespace App\Livewire;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Hash;
use Nette\Utils\Random;
use Illuminate\Support\Facades\Mail;
use Livewire\WithPagination;
use Illuminate\Support\Facades\File;

class Authors extends Component
{
    use WithPagination;
    public $name, $email, $username, $author_type, $direct_publisher;
    public $search;
    public $perPage = 2;
    public $selected_author_id;
    public $blocked = 0;
    protected $listeners = [
        'resetForms'
    ];

    // public function mount(){
    //     $this->resetPage();
    // }

    public function updatingSearch(){
        $this->resetPage();
    }

    public function resetForms(){
        $this->name = $this->email = $this->username = $this->author_type = $this->direct_publisher = null;
        $this->resetErrorBag();
    }

    public function addAuthor()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username|min:6|max:20',
            'author_type' => 'required',
            'direct_publisher' => 'required',
        ],[
            'author_type.required' => 'Choose author type',
            'direct_publisher.required' => 'Specify author publication access',

        ]);

        if ($this->isOnline()) {
            $default_password = Random::generate(8);
            $author = new User();
            $author->name = $this->name;
            $author->email = $this->email;
            $author->username = $this->username;
            $author->password = Hash::make($default_password);
            $author->type = $this->author_type;
            $author->direct_publish = $this->direct_publisher;
            $saved = $author->save();

            $data = array(
            'name' => $this->name,
            'username' => $this->username,
            'email' => $this->email,
            'password' =>$default_password,
            'url' => route('author.profile'),

            );

            $author_email = $this->email;
            $author_name = $this->name;

            if ($saved) {
                Mail::send('new-author-email-template', $data, function($message) use($author_email, $author_name){
                    $message->from('noreply@example.com','xxx');
                    $message->to($author_email, $author_name)
                            ->subject('Account creation');
                });
                session()->flash('success', 'New author has been added to blog');
                $this->name = $this->email = $this->username = $this->author_type = $this->direct_publisher = null;
                // $this->dispatchBrowserEvent('hide_add_author_modal');
            } else {
                session()->flash('fail', 'Something went wrong');
            }


        } else {
            session()->flash('fail', 'your are offline, check your connection and submit for again later');
        }

    }

    public function editAuthor($author){
        $this->selected_author_id = $author['id'];
        $this->name = $author['name'];
        $this->email = $author['email'];
        $this->username = $author['username'];
        $this->author_type = $author['type'];
        $this->direct_publisher = $author['direct_publish'];
        $this->blocked = $author['blocked'];

        // $this->dispatchBrowserEvent('showEditAuthorModal');
    }

    public function updateAuthor()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$this->selected_author_id,
            'username' => 'required|min:6|max:20|unique:users,username,'.$this->selected_author_id,
        ]);

        if ($this->selected_author_id) {
            $author = User::find($this->selected_author_id);
            $author->update([
                'name'=>$this->name,
                'email'=>$this->email,
                'username'=>$this->username,
                'type'=>$this->author_type,
                'blocked'=>$this->blocked,
                'direct_publish'=>$this->direct_publisher,
            ]);
            session()->flash('success', 'Author Details has been successfuly');
        }
    }

    public function deleteAuthor($id)
    {
        $author = User::find($id);

        if (!$author) {
            return redirect()->back()->withErrors(['fail' => 'Author not found']);
        }

        $author->delete();

        session()->flash('success', 'Author has been successfully deleted');
    }



    public function isOnline($site = "https://youtube.com/"){
        if (@fopen($site,"r")) {
            return true;
        } else {
            return false;
        }
    }

    public function render()
    {
        return view('livewire.authors',[
            'authors'=>User::search(trim($this->search))
                        ->where('id','!=',auth()->id() )->paginate($this->perPage),
        ]);

    }
}
