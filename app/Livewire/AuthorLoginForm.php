<?php

namespace App\Livewire;

use Illuminate\Http\Request;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthorLoginForm extends Component
{
    public $login_id, $password;
    public $returnUrl;

    public function mount(){
        $this->returnUrl = request()->returnUrl;
    }

    public function LoginHandler(){
        $filedType = filter_var($this->login_id, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if ($filedType == 'email') {
            $this->validate([
                'login_id' => 'required|email|exists:users,email',
                'password' => 'required|min:5',
            ],
            [
                'login_id.required' => 'Email or Username is required',
                'login_id.email' => 'Invalid email address',
                'login_id.exists' => 'This email is not registred',
                'password.required' => 'Password is required',
            ]);
        } else {
            $this->validate([
                'login_id' => 'required|email|exists:users,email',
                'password' => 'required|min:5',
            ],
            [
                'login_id.required' => 'Email or Username is required',
                'login_id.exists' => 'This email is not registred',
                'password.required' => 'Password is required',
            ]);
        }


        $creds = array($filedType=>$this->login_id, 'password'=>$this->password);

        if ( Auth::guard('web')->attempt($creds) ) {

            $checkUser = User::where($filedType, $this->login_id)->first();
            if ($checkUser->blocked == 1) {
                Auth::guard('web')->logout();
                return redirect()->route('author.login')->with('fail', 'Your account has been blocked');
            } else {
                if ( $this->returnUrl != null ) {
                    return redirect()->to( $this->returnUrl );
                } else {
                    return redirect()->route('author.home');
                }

            }
        }else {
            session()->flash('fail', 'Incorrect email or password');
        }
    }


    public function render()
    {
        return view('livewire.author-login-form');
    }
}
