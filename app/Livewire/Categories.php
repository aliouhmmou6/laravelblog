<?php

namespace App\Livewire;
use App\Models\Category;
use App\Models\Post;
use App\Models\SubCategory;
use Livewire\Component;
use Illuminate\Support\Str;

class Categories extends Component
{
    public $category_name;
    public $selected_category_id;
    public $updateCategoryMode = false;

    public $subcategory_name;
    public $parent_category = 0;
    public $selected_subcategory_id;
    public $updateSubCategoryMode = false;
    // protected $listeners = [
    //     'resetModalForm'
    // ];

    public function resetModalForm(){
        $this->resetErrorBag();
        $this->category_name = null;
        $this->subcategory_name = null;
        $this->parent_category = null;
    }
    public function addCategory()
    {
        $this->validate([
            'category_name' => 'required|unique:categories,category_name',
        ]);

        $category = new Category();
        $category->category_name = $this->category_name;
        $saved = $category->save();

        if ($saved) {
            // $this->dispatchBrowserEvent('hideCategoriesModal');
            $this->category_name = null;
            session()->flash('success', 'New Category has been added successfuly.');
        } else {
            session()->flash('fail', 'Something went wrong.');
        }
    }

    public function editCategory($id){
        $category = Category::findOrFail($id);
        $this->selected_category_id = $category->id;
        $this->category_name = $category->category_name;
        $this->updateCategoryMode = true;
        // $this->resetErrorBag();
    }

    public function updateCategory()
    {
        if ($this->selected_category_id) {
            $this->validate([
                'category_name' => 'required|unique:categories,category_name,'.$this->selected_category_id,
            ]);
            $category = Category::findOrFail($this->selected_category_id);
            $category->category_name = $this->category_name;
            $updated = $category->save();

            if ($updated) {
                $this->updateCategoryMode = false;
                session()->flash('success', 'Category has been updated successfuly.');
            } else {
                session()->flash('fail', 'Something went wrong.');
            }

        }
    }

    public function addSubCategory()
    {
        $this->validate([
            'parent_category' => 'required',
            'subcategory_name' => 'required|unique:sub_categories,subcategory_name',
        ]);

        $subcategory = new SubCategory();
        $subcategory->subcategory_name = $this->subcategory_name;
        $subcategory->slug = Str::slug($this->subcategory_name);
        $subcategory->parent_category = $this->parent_category;
        $saved = $subcategory->save();

        if ($saved) {
            // $this->dispatchBrowserEvent('hideCategoriesModal');
            $this->parent_category = null;
            $this->subcategory_name = null;
            session()->flash('success', 'New SubCategory has been added successfuly.');
        } else {
            session()->flash('fail', 'Something went wrong.');
        }
    }

    public function editSubCategory($id){
        $subcategory = SubCategory::findOrFail($id);
        $this->selected_subcategory_id = $subcategory->id;
        $this->parent_category = $subcategory->parent_category;
        $this->subcategory_name = $subcategory->subcategory_name;
        $this->updateSubCategoryMode = true;
        // $this->resetErrorBag();
    }

    public function updateSubCategory()
    {
        if ($this->selected_subcategory_id) {
            $this->validate([
                'parent_category' => 'required',
                'subcategory_name' => 'required|unique:sub_categories,subcategory_name,'.$this->selected_subcategory_id,
            ]);
            $subcategory = SubCategory::findOrFail($this->selected_subcategory_id);
            $subcategory->subcategory_name = $this->subcategory_name;
            $subcategory->slug = Str::slug($this->subcategory_name);
            $subcategory->parent_category = $this->parent_category;
            $updated = $subcategory->save();

            if ($updated) {
                $this->updateSubCategoryMode = false;
                session()->flash('success', 'SubCategory has been updated successfuly.');
            } else {
                session()->flash('info', 'Something went wrong.');
            }

        }
    }

    public function deleteCategory($id)
    {
        $category = Category::findOrFail($id);

        $subcategoryWithPosts = SubCategory::where('parent_category', $category->id)->whereHas('posts')->get();

        if ($subcategoryWithPosts->isNotEmpty()) {
            $totalPosts = 0;
            foreach ($subcategoryWithPosts as $subcat) {
                $totalPosts += $subcat->posts->count();
            }
            session()->flash('fail', 'This category has ' . $totalPosts . ' post(s) related to it and cannot be deleted.');

        } else {
            SubCategory::where('parent_category', $category->id)->delete();
            $category->delete();
            session()->flash('success', 'Category has been successfully deleted.');
        }
    }


    public function deleteSubCategory($id)
    {
        $subcategory = SubCategory::where('id',$id)->first();
        $posts = Post::where('category_id',$subcategory->subcategory_name)->get()->toArray();

        if ( !empty($posts) && count($posts) > 0 ) {
            session()->flash('fail', 'Category has been successfully deleted.');
        } else {
            $subcategory->delete();
            session()->flash('info', 'Category has been successfully deleted.');
        }


    }

    public function render()
    // {
    //     $categories = Category::all(); // Fetch all subcategories
    //     $subcategories = SubCategory::all(); // Fetch all subcategories
    //     $subcategories = $subcategories->filter(function ($subcategory) {
    //         return $subcategory->posts->count() > 0; // Filter subcategories with posts
    //     });

    //     return view('livewire.categories', [
    //         'categories' => $categories,
    //         'subcategories' => $subcategories
    //     ]);
    // }
    {

        return view('livewire.categories', [
            'categories'=>Category::orderBy('ordering','asc')->get(),
            'subcategories'=>SubCategory::orderBy('ordering','asc')->get(),
        ]);
    }
}
