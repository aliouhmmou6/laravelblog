<?php

namespace App\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class AuthorChangePasswordForm extends Component
{

    public $current_password, $new_password, $confirm_new_password;

    public function changePassword()
    {
        $this->validate([
            'current_password'=>[
                'required', function($attribute, $value, $fail) {
                    if (!Hash::check($value, User::find(auth('web')->id())->password)){
                        return  $fail(__('The current password is incorrect.'));
                    }
                },
            ],
            'new_password' => 'required|min:5|max:25',
            'confirm_new_password' => 'same:new_password'
        ],[
            'current_password.required' => 'Enter you current password',
            'new_password.required' => 'Enter new password',
            'confirm_new_password.same' => 'the confirm new password and password must much',

        ]);
        $query = User::find(auth('web')->id())->update([
            'password'=>Hash::make($this->new_password)
        ]);
        if ($query) {
            session()->flash('Your password has been successfully updated','success');
            $this->current_password = $this->new_password = $this->confirm_new_password = null;
        } else {
            session()->flash('fail', 'Something went wron');
        }


    }


    public function render()
    {
        return view('livewire.author-change-password-form');
    }
}
