<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Post;
use Livewire\WithPagination;

class AllPosts extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = null;
    public $author = null;
    public $category = null;
    public $orderBy = 'desc';

    // protected $listeners = [
    //     'deletePostAction'
    // ];

    // public function mount(){
    //     $this->resetPage();
    // }
    // public function updatingSearch(){
    //     $this->resetPage();
    // }
    // public function updatingCategory(){
    //     $this->resetPage();
    // }
    // public function updatingAuthor(){
    //     $this->resetPage();
    // }

    public function render()
    {
        return view('livewire.all-posts', [
            'posts' => auth()->user()->type ==1 ?
                    Post::search(trim($this->search))
                    ->when($this->category, function($query){
                        $query->where('category_id',$this->category);
                    })
                    ->when($this->author, function($query){
                        $query->where('author_id',$this->author);
                    })
                    // ->when($this->orderBy, function($query){
                    //     $query->orderBy('id',$this->orderBy);
                    // })
                    ->paginate($this->perPage) :
                    Post::search(trim($this->search))
                    ->when($this->category, function($query){
                        $query->where('category_id',$this->category);
                    })
                    ->where('author_id',auth()->id())
                    // ->when($this->orderBy, function($query){
                    //     $query->orderBy('id',$this->orderBy);
                    // })
                    ->paginate($this->perPage)
        ]);
    }

    // public function deletePostAction($id)
    // {
    //     $post = Post::find($id);
    //     $path = "images/post_images/";
    //     $featured_image = $post->new_filename;

    //     $delete_post = $post->delete();
    //     if ($delete_post) {
    //         $this->with('success', 'Post has been successfulu deleted');
    //     } else {
    //         $this->with('fail', 'something went wrong');
    //     }

    // }

    public function deletePost($id)
    {
        $post = Post::find($id);

        if (!$post) {
            return redirect()->back()->withErrors(['fail' => 'post not found']);
        }

        $post->delete();

        session()->flash('success', 'post has been successfully deleted');
    }
}
