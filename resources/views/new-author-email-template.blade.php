
<br>
-- Hi <b>{{ $name }} </b><br><br>
You Account has been created on Larablog.
You can use the following credentials.
    
<br>
<b>Username:</b>{{ $username }} <br>
<b>Email:</b>{{ $email }} <br>
<b>Password:</b>{{ $password }} <br>
<br>
<a href="{{ $url }}">Go to your profile page</a>
<br> <br>
<b>Note :</b>Its important to change this default password after login in to system on the first time.

<br> <br>
Thank you 