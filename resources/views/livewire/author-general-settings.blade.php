<div>

    <form wire:submit.prevent='updateGeneralSettings()' method="POST">
        <div class="row">
            <div class="col-md-6">
                <div class="mb-3">
                    <label class="form-label">Blog Name</label>
                    <input type="text" class="form-control" placeholder="Enter Blog Name" wire:model='blog_name'>
                    <span class="text-danger">
                        @error('blog_name')
                        {{ $message }}
                        @enderror
                    </span>
                </div>
                <div class="mb-3">
                    <label class="form-label">Blog Email</label>
                    <input type="text" class="form-control" placeholder="Enter Blog Email" wire:model='blog_email'>
                    <span class="text-danger">
                        @error('blog_email')
                        {{ $message }}
                        @enderror
                    </span>
                </div>
                <div class="mb-3">
                    <label class="form-label">Blog Description</label>
                    <textarea class="form-control" rows="3" cols="3" wire:model='blog_description'>

                    </textarea>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Save Change</button>
    </form>

</div>
