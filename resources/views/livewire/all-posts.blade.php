<div>

    <div class="row">
        <div class="col-md-6 mb-3">
            <label for="" class="form-label">Search</label>
            <input type="text" class="form-control" placeholder="keyword...." wire:model='search' />
        </div>
        <div class="col-md-2 mb-3">
            <label for="" class="form-label">Category</label>
            <select id="" class="form-select" wire:model='category'>
                <option value="">---No Selected----</option>
                @foreach (\App\Models\SubCategory::all() as $category)
                    <option value="{{ $category->id }}">{{ $category->subcategory_name }}</option>
                @endforeach
                {{-- @foreach (\App\Models\SubCategory::whereHas('posts')->get() as $category)
                    <option value="{{ $category->id }}">{{ $category->subcategory_name }}</option>
                @endforeach --}}
            </select>
        </div>
        @if (auth()->user()->type == 1)
        <div class="col-md-2 mb-3">
            <label for="" class="form-label">Author</label>
            <select id="" class="form-select" wire:model='author'>
                <option value="">---No Selected----</option>
                @foreach (\App\Models\User::whereHas('posts')->get() as $author)
                <option value="{{ $author->id }}">{{ $author->name }}</option>
                @endforeach
            </select>
        </div>
        @endif

        <div class="col-md-2 mb-3">
            <label for="" class="form-label">orderBy</label>
            <select id="" class="form-select" wire:model='orderBy'>
                <option value="asc">ASC</option>
                <option value="desc">DESC</option>
            </select>
        </div>
    </div>

    <div class="row row-cards">

        @forelse ($posts as $post)
        <div class="col-md-6 col-lg-3">
            <div class="card">
                <img src="/images/post_images/{{$post->featured_image}}" alt="" class="card-mg-top"
                style="width: 100%; height: 200px; object-fit: cover;">
                <div class="card-body p-2">
                    <h3 class="m-0 mb-1">{{$post->post_title}}</h3>
                </div>
                <div class="d-flex">
                    <a href="{{ route('author.posts.edit-post',['post_id'=>$post->id]) }}" class="card-btn">Edit</a>
                    <a href="#" wire:click.prevent='deletePost({{ $post->id }})' class="card-btn">Delete</a>
                </div>
            </div>
        </div>
        @empty
            <span class="text-danger">No post(s) found</span>
        @endforelse

    </div>

    <div class="d-block mt-2">
        {{ $posts->links('livewire::simple-bootstrap') }}
    </div>

</div>



{{-- @section('scripts')

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    function confirmDelete(productId) {
        if (confirm("Are you sure you want to delete this post?")) {
            document.getElementById(`delete-form-${productId}`).submit();
        }
    }
</script>
@endsection --}}
