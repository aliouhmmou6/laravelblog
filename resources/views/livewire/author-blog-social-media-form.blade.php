<div>

    <form wire:submit.prevent='updateBlogSocialMedia()' method="POST">
        <div class="row">
            <div class="col-md-6">
                <div class="mb-3">
                    <label for="">Facebook</label>
                    <input type="text" class="form-control" wire:model='facebook_url'
                        placeholder="Facebook URL....."/>
                    @error('facebook_url')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="mb-3">
                    <label for="">Instagram</label>
                    <input type="text" class="form-control" wire:model='instagram_url'
                        placeholder="Instagram URL....."/>
                    @error('instagram_url')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="mb-3">
                    <label for="">Youtube</label>
                    <input type="text" class="form-control" wire:model='youtube_url'
                        placeholder="Youtube URL....."/>
                    @error('youtube_url')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="mb-3">
                    <label for="">Linkedin</label>
                    <input type="text" class="form-control" wire:model='linkedin_url'
                        placeholder="Linkedin URL....."/>
                    @error('linkedin_url')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
</form>

</div>
