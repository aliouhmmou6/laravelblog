@extends('back.layouts.pages-layout')
@section('pageTitle', @isset($pageTitle) ? $pageTitle : 'Settings')
@section('content')

    <div class="row-align-items-center">
        <div class="col">
            <div class="page-title">
                Settings
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs card-header-tabs" data-bs-toggle="tabs" role="tablist">
            <li class="nav-item" role="presentation">
              <a href="#tabs-home-8" class="nav-link active" data-bs-toggle="tab">General Setting</a>
            </li>
            <li class="nav-item" role="presentation">
              <a href="#tabs-profile-8" class="nav-link" data-bs-toggle="tab">Logo & Favicon</a>
            </li>
            <li class="nav-item" role="presentation">
              <a href="#tabs-activity-8" class="nav-link" data-bs-toggle="tab">Social Media</a>
            </li>
          </ul>
        </div>
        <div class="card-body">
          <div class="tab-content">
            <div class="tab-pane fade active show" id="tabs-home-8" role="tabpanel">
              <div>
                @livewire('author-general-settings')
              </div>
            </div>
            <div class="tab-pane fade" id="tabs-profile-8" role="tabpanel">
              <div>
                <div class="row">
                    <div class="col-md-6">
                        <h3>Set Blog favicon</h3>
                        <div class="mb-2" style="max-width: 200px">
                            <img src="" alt="" class="img-thumbnail" id="logo-image-preview">
                        </div>
                        <form action="" method="POST" id="changeBlogLogoForm">
                            @csrf
                            <div class="mb-2">
                                <input type="file" class="form-control" name="blog_logo">
                            </div>
                            <button class="btn btn-primary">Change Logo</button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h3>Set Blog logo</h3>
                        <div class="mb-2" style="max-width: 100px">
                            <img src="" alt="" class="img-thumbnail" id="logo-image-preview">
                        </div>
                        <form action="" method="POST" id="changeBlogFaviconForm">
                            @csrf
                            <div class="mb-2">
                                <input type="file" class="form-control" name="blog_logo">
                            </div>
                            <button class="btn btn-primary">Change Favicon</button>
                        </form>
                    </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="tabs-activity-8" role="tabpanel">
              <div>
                @livewire('author-blog-social-media-form')
              </div>
            </div>
          </div>
        </div>
    </div>

@endsection
