@extends('back.layouts.pages-layout')
@section('pageTitle', @isset($pageTitle) ? $pageTitle : 'Categories')
@section('content')

<div class="page-header d-print-none">
    <div class="row align-items-center">
        <div class="col">
          <h2 class="page-title">
            Categories & SubCategories
          </h2>
        </div>
    </div>
</div>

@livewire('categories')

@endsection


{{-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script src="https://cdn.dataTables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.dataTables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.dataTables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.dataTables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js"></script> --}}

{{-- <script>
    $('#categories_modal, #categories_modal').on('hidden.bs.modal', function(e){
        Livewire.emit('resetModalForm');
    });
</script> --}}

